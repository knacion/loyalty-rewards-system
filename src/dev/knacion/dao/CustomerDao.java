package dev.knacion.dao;

import dev.knacion.model.Customer;

import java.sql.*;
import java.util.List;
import java.util.UUID;

public class CustomerDao {

    private final String DB_BASE_URL = "jdbc:mariadb://localhost:3306/";
    private final String DB_NAME = "casestudydb";
    private final String USERNAME = "root";
    private final String PASSWORD = "root";

    private Connection connection;

    public CustomerDao(){
        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        try {
            connection = DriverManager.getConnection(DB_BASE_URL+DB_NAME, USERNAME, PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Customer> getAllCustomer(){
        List<Customer> customers = null;

        try {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM " + DB_NAME + ".customer_table");

            ResultSet rs = ps.executeQuery();

            while(rs.next()){
                Customer customer = insertData(rs);
                customers.add(customer);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return customers;
    }

    public Customer getCustomerById(UUID customerId){
        Customer customer = null;

        try {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM " + DB_NAME + ".customer_table WHERE customer_id = ?");

            ps.setString(1, customerId.toString());
            ResultSet rs = ps.executeQuery();

            if (rs.next()){
                customer = insertData(rs);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return customer;
    }

    private Customer insertData(ResultSet rs) throws SQLException {
        Customer customer = new Customer();
        customer.setCustomer_id(UUID.fromString(rs.getString("customer_id")));
        customer.setFirstName(rs.getString("first_name"));
        customer.setMiddleName(rs.getString("middle_name"));
        customer.setLastName(rs.getString("last_name"));
        customer.setBirthDate(rs.getDate("birthdate"));
        customer.setCardExpirationDate(rs.getDate("card_expiration_date"));
        return customer;
    }
}
