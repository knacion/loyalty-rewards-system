package dev.knacion.dao;

import dev.knacion.model.UserAccount;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserAccountDao {

    private final String DB_BASE_URL = "jdbc:mariadb://localhost:3306/";
    private final String DB_NAME = "casestudydb";
    private final String USERNAME = "root";
    private final String PASSWORD = "root";


    private Connection connection;

    public UserAccountDao() {
        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        try {
            connection = DriverManager.getConnection(DB_BASE_URL + DB_NAME, USERNAME, PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public List<UserAccount> getAllUsers() {
        List<UserAccount> userList = null;

        try {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM user_table");
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                String username = rs.getString("username");
                String password = rs.getString("password");
                userList = new ArrayList<>();
                userList.add(new UserAccount(username, password));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return userList;
    }

    public UserAccount getUser(String username, String password) {
        UserAccount user = null;

        try {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM " + DB_NAME + ".user_table WHERE username = ? AND password = ?");
            ps.setString(1, username);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();

            while(rs.next()){
                String dbUsername = rs.getString("username");
                String dbPassword = rs.getString("password");
                user = new UserAccount(dbUsername, dbPassword);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


        return user;
    }

}
