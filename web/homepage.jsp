<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>LeWard | Home</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="resources/css/bulma.min.css"/>
  </head>
  <body>

    <header>
      <nav class="navbar is-transparent">
        <div class="navbar-brand">
          <a href="homepage.jsp">
            <h2 class="title">LeWards System</h2>
          </a>
        </div>
      </nav>
    </header>

    <div class="hero is-centered is-fullheight">
      <form action="members" method="get">
        <button class="button" type="submit" name="button">Add Member</button>
      </form>
    </div>

  </body>
</html>